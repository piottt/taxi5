package apm.muei.taxi5;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Perfil");
    }

    public void onclickBtnHistorico(View view) {
        Toast.makeText(this, "Abriendo historico de viajes", Toast.LENGTH_SHORT).show();
    }

    public void onclickBtnEditarPerfil(View view) {
        Toast.makeText(this, "Abriendo editar perfil del usuario", Toast.LENGTH_SHORT).show();
    }

    public void onclickBtncerrarSesion(View vista) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

}