package apm.muei.taxi5;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

public class InfTaxiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inf_taxi);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    public void onPagarClick (View view) {
        Intent intent = new Intent(InfTaxiActivity.this, PayActivity.class);
        startActivity(intent);
    }

    public void onClickBtnCancelar(View view) {
        Toast.makeText(this, "Cancelar viaje", Toast.LENGTH_SHORT).show();
    }

    public void onclickBtnCompartir(View view) {
        Switch simpleSwitch = (Switch) findViewById(R.id.switch1);
        Boolean switchState = simpleSwitch.isChecked();

        if (switchState) {
            Toast.makeText(this, "Compartir viaje", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Dejar de compartir viaje", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickBtnLlamarTaxista(View view) {
        Toast.makeText(this, "Llamando al taxista", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(Intent.ACTION_DIAL, null);
        startActivity(i);
    }
}
